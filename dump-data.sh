#!/usr/bin/env bash

SCRIPT_DIR=`dirname $0`

source "${SCRIPT_DIR}/config.sh"

for USERID in $USERIDS; do
    URL="https://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=${KEY}&steamid=${USERID}&format=json"

    OUTFILE="${DATADIR}/${USERID}/`date --iso`.json"
    DIRNAME=`dirname "${OUTFILE}"`
    mkdir -p "${DIRNAME}"

    curl $URL > $OUTFILE
done
