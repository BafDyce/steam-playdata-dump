# Easily dump your recently played steam data

A simple bash script that dumps the information from [GetRecentlyPlayedGames](
https://developer.valvesoftware.com/wiki/Steam_Web_API#GetRecentlyPlayedGames_.28v0001.29)
into a `.json` file.

Written to be easily used in a cron job.

## How to set up

1. Clone the repo
2. Create the config file
    1. `cp config-template.sh config.sh`
    2. Fill out `config.sh`
    3. Get your Steam API key from here: https://steamcommunity.com/dev/apikey
    4. Find out the steam ID of the profile you want data for (e.g. with an
        online service)
    5. **Change the permissions of the config file**: `chmod 400 config.sh`
    6. **This is IMPORTANT to protect your API key!!**
3. Run the script (`./dump-data.sh`)
4. ???
5. Profit!

## Run daily with a cron job

1. Follow the setup steps and ensure that the script is working (i.e. that you
    indeed get files with the expected data)
2. Setup the cron job:
    1. `crontab -e`
    2. Add the following line:
        `1 0 * * * bash /path/to/the/repo/dump-data.sh`
    3. This will run the script daily, 1 minute after midnight.

## Disclaimer

```
This project is not affiliated with Steam or Valve, it was privately developed
and is independent.
```
